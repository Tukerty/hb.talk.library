/**
 * 
 * @param {string} username user identificator
 * @param {string} webSocketAddress address of signaling websocket server
 * @param {string} inputVideoId id of video tag, where input video should be
 * @param {string} outputVideoId id of video tag, where output video should be
 */

function hTalk(username,webSocketAddress, stunAddress, videoInput, videoOutput) {
    var talk = this
    this.username = username
    this.videoInput = videoInput
    this.videoOutput = videoOutput
/* */
    this.register = function() {
        setRegisterState(REGISTERING);
        var message = {
            id: 'register',
            name: username
        };
        sendMessage(message);
    }
    this.stop = function(message) {
        console.log('stopped')
        setCallState(NO_CALL);
        if (webRtcPeer) {
            webRtcPeer.dispose();
            webRtcPeer = null;

            if (!message) {
                var message = {
                    id: 'stop'
                }
                sendMessage(message);
            }
        }
        hideSpinner(videoInput, videoOutput);
    }
    this.call = function(peer) {

        if (document.getElementById('peer').value == '') {
            window.alert("You must specify the peer name");
            return;
        }

        setCallState(PROCESSING_CALL);
                
        showSpinner(videoInput, videoOutput);

        var options = {
            localVideo: videoInput,
            remoteVideo: videoOutput,
            onicecandidate: onIceCandidate,
        }

        webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options, function (
            error) {
            if (error) {
                console.log(error);
                setCallState(NO_CALL);
            }

            this.generateOffer(function (error, offerSdp) {
                if (error) {
                    console.log(error);
                    setCallState(NO_CALL);
                }
                var message = {
                    id: 'call',
                    from: username,
                    to: document.getElementById('peer').value,
                    sdpOffer: offerSdp
                };
                sendMessage(message);
            });
        });

    }
    this.checkIfUserOnline = function(username){

        var message = {
            id: 'checkIfUserOnline',
            name: username
        };
        sendMessage(message);
    }

    var ws = new WebSocket('wss://' + webSocketAddress + '/one2one');
    var webRtcPeer;

    var registerName = null;
    const NOT_REGISTERED = 0;
    const REGISTERING = 1;
    const REGISTERED = 2;
    var registerState = null

    function setRegisterState(nextState) {
        switch (nextState) {
            case NOT_REGISTERED:
                break;

            case REGISTERING:
                break;

            case REGISTERED:
                setCallState(NO_CALL);
                break;

            default:
                return;
        }
        registerState = nextState;
    }

    const NO_CALL = 0;
    const PROCESSING_CALL = 1;
    const IN_CALL = 2;
    var callState = null

    function setCallState(nextState) {
        switch (nextState) {
            case NO_CALL:
                break;

            case PROCESSING_CALL:
                break;
            case IN_CALL:
                break;
            default:
                return;
        }
        callState = nextState;
    }

    window.onload = function () {
        setRegisterState(NOT_REGISTERED);
    }

    window.onbeforeunload = function () {
        ws.close();
    }

    ws.onopen = function (){
        talk.register();    
    }

    ws.onmessage = function (message) {
        var parsedMessage = JSON.parse(message.data);
        console.log('Received message: ' + message.data);

        switch (parsedMessage.id) {
            case 'registerResponse':
                resgisterResponse(parsedMessage);
                break;
            case 'callResponse':
                callResponse(parsedMessage);
                break;
            case 'incomingCall':
                incomingCall(parsedMessage);
                break;
            case 'startCommunication':
                startCommunication(parsedMessage);
                break;
            case 'stopCommunication':
                console.log("Communication ended by remote peer");
                talk.stop(true);
                break;
            case 'checkIfUserOnlineResponce':
                console.log(parsedMessage.message)
                break;
            case 'iceCandidate':
                webRtcPeer.addIceCandidate(parsedMessage.candidate)
                break;
            default:
                console.log('Unrecognized message', parsedMessage);
        }
    }

    function resgisterResponse(message) {
        if (message.response == 'accepted') {
            setRegisterState(REGISTERED);
        } else {
            setRegisterState(NOT_REGISTERED);
            var errorMessage = message.message ? message.message :
                'Unknown reason for register rejection.';
            console.log(errorMessage);
            alert('Error registering user. See console for further information.');
        }
    }

    function callResponse(message) {
        if (message.response != 'accepted') {
            console.log('Call not accepted by peer. Closing call');
            var errorMessage = message.message ? message.message :
                'Unknown reason for call rejection.';
            console.log(errorMessage);
            talk.stop(true);
        } else {
            setCallState(IN_CALL);
            webRtcPeer.processAnswer(message.sdpAnswer);
        }
    }

    function startCommunication(message) {
        setCallState(IN_CALL);
        webRtcPeer.processAnswer(message.sdpAnswer);
    }

    function incomingCall(message) {
        // If bussy just reject without disturbing user
        if (callState != NO_CALL) {
            var response = {
                id: 'incomingCallResponse',
                from: message.from,
                callResponse: 'reject',
                message: 'bussy'

            };
            return sendMessage(response);
        }

        setCallState(PROCESSING_CALL);
        if (confirm('User ' + message.from +
                ' is calling you. Do you accept the call?')) {
            showSpinner(videoInput, videoOutput);

            var options = {
                localVideo: videoInput,
                remoteVideo: videoOutput,
                onicecandidate: onIceCandidate
            }

            webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options,
                function (error) {
                    if (error) {
                        console.log(error);
                        setCallState(NO_CALL);
                    }

                    this.generateOffer(function (error, offerSdp) {
                        if (error) {
                            console.log(error);
                            setCallState(NO_CALL);
                        }
                        var response = {
                            id: 'incomingCallResponse',
                            from: message.from,
                            callResponse: 'accept',
                            sdpOffer: offerSdp
                        };
                        sendMessage(response);
                    });
                });

        } else {
            var response = {
                id: 'incomingCallResponse',
                from: message.from,
                callResponse: 'reject',
                message: 'user declined'
            };
            sendMessage(response);
            talk.stop(true);
        }
    }

    function sendMessage(message) {
        var jsonMessage = JSON.stringify(message);
        console.log('Senging message: ' + jsonMessage);
        ws.send(jsonMessage);
    }

    function onIceCandidate(candidate) {
        console.log('Local candidate' + JSON.stringify(candidate));

        var message = {
            id: 'onIceCandidate',
            candidate: candidate
        }
        sendMessage(message);
    }

    function showSpinner() {
        console.log(arguments)
        for (var i = 0; i < arguments.length; i++) {
            arguments[i].poster = './img/transparent-1px.png';
            arguments[i].style.background = 'center transparent url("./img/spinner.gif") no-repeat';
        }
    }

    function hideSpinner() {
        for (var i = 0; i < arguments.length; i++) {
            arguments[i].src = '';
            arguments[i].poster = './img/webrtc.png';
            arguments[i].style.background = '';
        }
    }

    /**
     * Lightbox utility (to display media pipeline image in a modal dialog)
     */
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    kurento(640,480,15);
}
